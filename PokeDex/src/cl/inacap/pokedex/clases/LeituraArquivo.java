package cl.inacap.pokedex.clases;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class LeituraArquivo {
    public static Scanner sc = new Scanner(System.in);
    public static String Read  (String caminho) { // classe de leitura
        String conteudo = "";
        try {
            FileReader arq = new FileReader(caminho);
            BufferedReader LerArq = new BufferedReader(arq);
            String linha = "";
            try {
                    linha = LerArq.readLine();
                    while (linha!=null) {
                            conteudo += linha+",";
                            linha = LerArq.readLine();
                    }
                    arq.close();
            } catch (IOException ex) {
                    conteudo = "Erro: não foi possível ler o Arquivo";
            }

    } catch (FileNotFoundException ex) {
            conteudo = "Erro: arquivo não encontrado";
    }
    if (conteudo.contains("Erro")) 
            return ""; 
    else 
        return conteudo; 		
    }
}
