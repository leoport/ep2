/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.inacap.pokedex.clases;
import static cl.inacap.pokedex.clases.LeituraArquivo.sc;
import java.util.ArrayList;
import static pokedex.PokeDex.clear;
import static pokedex.PokeDex.pause;
//import static pokedex.PokeDex.pokemons;

/**
 *
 * @author leonardo
 */
public class Pokemon extends Pessoa{
    private String id;
    private String tipo1;
    private String tipo2;
    private String total;
    private String hp;
    private String ataque;
    private String defesa;
    private String sp_ataque;
    private String sp_defesa;
    private String velocidade;
    private String geracao;
    private String legendario;
    private String experiencia;
    private String altura;
    private String peso;
    private String habilidade_1;
    private String habilidade_2;
    private String habilidade_3;
    private String movimento_1;
    private String movimento_2;
    private String movimento_3;
    private String movimento_4;
    private String movimento_5;
    private String movimento_6;
    private String movimento_7;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getTipo1() {
        return tipo1;
    }

    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    public String getTipo2() {
        return tipo2;
    }

    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public String getAtaque() {
        return ataque;
    }

    public void setAtaque(String ataque) {
        this.ataque = ataque;
    }

    public String getDefesa() {
        return defesa;
    }

    public void setDefesa(String defesa) {
        this.defesa = defesa;
    }

    public String getSp_ataque() {
        return sp_ataque;
    }

    public void setSp_ataque(String sp_ataque) {
        this.sp_ataque = sp_ataque;
    }

    public String getSp_defesa() {
        return sp_defesa;
    }

    public void setSp_defesa(String sp_defesa) {
        this.sp_defesa = sp_defesa;
    }

    public String getVelocidade() {
        return velocidade;
    }

    public void setVelocidade(String velocidade) {
        this.velocidade = velocidade;
    }

    public String getGeracao() {
        return geracao;
    }

    public void setGeracao(String geracao) {
        this.geracao = geracao;
    }

    public String getLegendario() {
        return legendario;
    }

    public void setLegendario(String legendario) {
        this.legendario = legendario;
    }

    public String getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(String experiencia) {
        this.experiencia = experiencia;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getHabilidade_1() {
        return habilidade_1;
    }

    public void setHabilidade_1(String habilidade_1) {
        this.habilidade_1 = habilidade_1;
    }

    public String getHabilidade_2() {
        return habilidade_2;
    }

    public void setHabilidade_2(String habilidade_2) {
        this.habilidade_2 = habilidade_2;
    }

    public String getHabilidade_3() {
        return habilidade_3;
    }

    public void setHabilidade_3(String habilidade_3) {
        this.habilidade_3 = habilidade_3;
    }

    public String getMovimento_1() {
        return movimento_1;
    }

    public void setMovimento_1(String movimento_1) {
        this.movimento_1 = movimento_1;
    }

    public String getMovimento_2() {
        return movimento_2;
    }

    public void setMovimento_2(String movimento_2) {
        this.movimento_2 = movimento_2;
    }

    public String getMovimento_3() {
        return movimento_3;
    }

    public void setMovimento_3(String movimento_3) {
        this.movimento_3 = movimento_3;
    }

    public String getMovimento_4() {
        return movimento_4;
    }

    public void setMovimento_4(String movimento_4) {
        this.movimento_4 = movimento_4;
    }

    public String getMovimento_5() {
        return movimento_5;
    }

    public void setMovimento_5(String movimento_5) {
        this.movimento_5 = movimento_5;
    }

    public String getMovimento_6() {
        return movimento_6;
    }

    public void setMovimento_6(String movimento_6) {
        this.movimento_6 = movimento_6;
    }

    public String getMovimento_7() {
        return movimento_7;
    }

    public void setMovimento_7(String movimento_7) {
        this.movimento_7 = movimento_7;
    }
    
    
    public static ArrayList<Pokemon> registrar(String arquivoCSV1, String arquivoCSV2, ArrayList<Pokemon> pokemons) {
        String informacoes = LeituraArquivo.Read(arquivoCSV1);
        String informacoes2 = LeituraArquivo.Read(arquivoCSV2);
        int k=0, j=0;
        for (int i=0; i<722; i++) {
            Pokemon poke = new Pokemon();
            poke.setId(informacoes.split(",")[k]);
            poke.setNome(informacoes.split(",")[k+1]);
            poke.setTipo1(informacoes.split(",")[k+2]);
            poke.setTipo2(informacoes.split(",")[k+3]);
            poke.setTotal(informacoes.split(",")[k+4]);
            poke.setHp(informacoes.split(",")[k+5]);
            poke.setAtaque(informacoes.split(",")[k+6]);
            poke.setDefesa(informacoes.split(",")[k+7]);
            poke.setSp_ataque(informacoes.split(",")[k+8]);
            poke.setSp_defesa(informacoes.split(",")[k+9]);
            poke.setVelocidade(informacoes.split(",")[k+10]);
            poke.setGeracao(informacoes.split(",")[k+11]);
            poke.setLegendario(informacoes.split(",")[k+12]);
            poke.setExperiencia(informacoes2.split(",")[j+2]);
            poke.setAltura(informacoes2.split(",")[j+3]);
            poke.setPeso(informacoes2.split(",")[j+4]);
            poke.setHabilidade_1(informacoes2.split(",")[j+5]);
            poke.setHabilidade_2(informacoes2.split(",")[j+6]);
            poke.setHabilidade_3(informacoes2.split(",")[j+7]);
            poke.setMovimento_1(informacoes2.split(",")[j+8]);
            poke.setMovimento_2(informacoes2.split(",")[j+9]);
            poke.setMovimento_3(informacoes2.split(",")[j+10]);
            poke.setMovimento_4(informacoes2.split(",")[j+11]);
            poke.setMovimento_5(informacoes2.split(",")[j+12]);
            poke.setMovimento_6(informacoes2.split(",")[j+13]);
            poke.setMovimento_7(informacoes2.split(",")[j+14]);
            pokemons.add(poke);
            k+=13;
            j+=15;
        }
       return pokemons;
    }
    public static void mostrar_pokemon(Pokemon pokem) {          
            System.out.println("Id: " + pokem.getId());
            System.out.println("Nome: " + pokem.getNome());
            System.out.println("Tipo1: " + pokem.getTipo1());
            System.out.println("Tipo2: " + pokem.getTipo2());
            System.out.println("Total: " + pokem.getTotal());
            System.out.println("Hp: " + pokem.getHp());
            System.out.println("Ataque: " + pokem.getAtaque());
            System.out.println("Defesa: " + pokem.getDefesa());
            System.out.println("Sp_ataque: " + pokem.getSp_ataque());
            System.out.println("Sp_defesa: " + pokem.getSp_defesa());
            System.out.println("Velocidade: " + pokem.getVelocidade());
            System.out.println("Geracao: " + pokem.getGeracao());
            System.out.println("Legendario: " + pokem.getLegendario());
            System.out.println("Experiencia: " + pokem.getExperiencia());
            System.out.println("Altura: " + pokem.getAltura());
            System.out.println("Peso: " + pokem.getPeso());
            System.out.println("Habilidade_1: " + pokem.getHabilidade_1());
            System.out.println("Habilidade_2: " + pokem.getHabilidade_2());
            System.out.println("Habilidade_3: " + pokem.getHabilidade_3());
            System.out.println("Movimento_1: " + pokem.getMovimento_1());
            System.out.println("Movimento_2: " + pokem.getMovimento_2());
            System.out.println("Movimento_3: " + pokem.getMovimento_3());
            System.out.println("Movimento_4: " + pokem.getMovimento_4());
            System.out.println("Movimento_5: " + pokem.getMovimento_5());
            System.out.println("Movimento_6: " + pokem.getMovimento_6());
            System.out.println("Movimento_7: " + pokem.getMovimento_7()); 
            System.out.println("-------------------------------"); 
    }
    public static void buscar_pokemon(ArrayList<Pokemon> pokemons) {
        ArrayList<Pokemon> opcoesDebusca = new ArrayList<Pokemon>();
        clear();
        System.out.println("Digite o nome ou tipo do Pokemon que deseja capturar.");
        String nome_tipo = sc.nextLine().trim();       
        for (int i=1; i<722; i++) {
            if (pokemons.get(i).getNome().equals(nome_tipo) || pokemons.get(i).getTipo1().equals(nome_tipo)
                 || pokemons.get(i).getTipo2().equals(nome_tipo)  ){
               opcoesDebusca.add(pokemons.get(i));
            }              
        }
        
        for (int i=0; i < opcoesDebusca.size(); i++){
            clear();
            mostrar_pokemon(opcoesDebusca.get(i));   
        }
        System.out.println("Escolha o ID do seu pokemon.");
        System.out.println();
        
       // String ID = sc.nextLine().trim();       
    }
        
}
