/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.inacap.pokedex.clases;
import static cl.inacap.pokedex.clases.LeituraArquivo.sc;
import static cl.inacap.pokedex.clases.Pokemon.buscar_pokemon;
import static cl.inacap.pokedex.clases.Pokemon.mostrar_pokemon;
import java.util.ArrayList;
import java.util.Vector;
import static pokedex.PokeDex.clear;
import static pokedex.PokeDex.pause;

/**
 *
 * @author leonardo
 */
public class Treinador extends Pessoa {

    
    private int numero_depokemons;

    public int getNumero_depokemons() {
        return numero_depokemons;
    }

    public void setNumero_depokemons(int numero_depokemons) {
        this.numero_depokemons = numero_depokemons;
    }
    private int registrado=0;

    public int getRegistrado() {
        return registrado;
    }

    public void setRegistrado(int registrado) {
        this.registrado = registrado;
    }
                        
    public ArrayList<Pokemon> pokemonz = new ArrayList<Pokemon>();

         
    public static ArrayList<Treinador> registrar(ArrayList<Treinador> treinadores,
            ArrayList<Pokemon> pokemons){        
        
        Treinador trainee = new Treinador();
        clear();
        System.out.println("Digite Seu nome!");
        String nome_treinador = sc.nextLine().trim();
        trainee.setNome(nome_treinador);
        
        System.out.println("Pressione Enter para registrar seus 3 primeiros pokemons.");
        for (int i=1; i < 4; i++){
            buscar_pokemon(pokemons); // pesquisa os pokemons e mostra ao treinador
          //  clear();
            System.out.println("Digite o ID do pokemon " + i);
            String id_pokemon = sc.nextLine().trim();        
           // trainee.pokemonz.add(localizar_pokemon(id_pokemon, pokemons));
        }        
        treinadores.add(trainee);
        return treinadores;
    } 
    
    
    public static void listar_pokemon(ArrayList<Pokemon> pokemons){
        for (int i=0; i<pokemons.size(); i++){
            mostrar_pokemon(pokemons.get(i));
        }   
    }
    
    public static Pokemon localizar_pokemon(String id, ArrayList<Pokemon> 
            pokemons) {
        Pokemon poke = new Pokemon();
        for (int i=0; i<pokemons.size(); i++){
            if (pokemons.get(i).getId().equals(id)){
                poke = pokemons.get(i);
            }
        }        
        return poke;
    }
}
