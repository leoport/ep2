/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex;
//import cl.inacap.pokedex.clases.LeituraArquivo;
import static cl.inacap.pokedex.clases.LeituraArquivo.sc;
import cl.inacap.pokedex.clases.Pokemon;
import static cl.inacap.pokedex.clases.Pokemon.buscar_pokemon;
import static cl.inacap.pokedex.clases.Pokemon.mostrar_pokemon;
import static cl.inacap.pokedex.clases.Pokemon.registrar;
import cl.inacap.pokedex.clases.Treinador;
import cl.inacap.pokedex.clases.Treinador;
import static cl.inacap.pokedex.clases.Treinador.listar_pokemon;
import static cl.inacap.pokedex.clases.Treinador.registrar;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.Scanner;
//import java.util.Vector;
/**
 *
 * @author leonardo
 */
public class PokeDex {
    /**
     * @param args the command line arguments
     */ 
    /**
     *
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        
        String arquivoCSV1 = "../data/csv_files/POKEMONS_DATA_1.csv";
        String arquivoCSV2 = "../data/csv_files/POKEMONS_DATA_2.csv";
        
        ArrayList<Pokemon> pokemons = new ArrayList<Pokemon>();
        ArrayList<Treinador> treinadores = new ArrayList<Treinador>();
                       
        registrar(arquivoCSV1,arquivoCSV2, pokemons); // registrar Pokemons      
         
        boolean continuar = true;
        while (continuar){
            int posicao = 0, k=0;
            
            clear();            
            System.out.println("-----------------------");
            System.out.println("I BEM-VINDO À POKEDEX I");
            System.out.println("-----------------------");
            System.out.println("ESCOLHA UMA OPÇÃO QUE DESEJA FAZER");
            System.out.println();
            System.out.println("SE VOCÊ FOR REGISTRADO:");
            System.out.println("1. Capturar um novo Pokemon.");
            System.out.println("2. Mostrar seus Pokemons.");
            System.out.println();
            System.out.println("CASO NÃO SEJA REGISTRADO:");
            System.out.println("3. Registrar-se.");
            System.out.println();
            System.out.println("0. Sair");

            String opcao = sc.nextLine().trim();
            switch (opcao){
                case "1":
                    System.out.println("Por favor, identfique-se");
                    String nom_treinador = sc.nextLine().trim();
                    if (verifica_treinador(nom_treinador, treinadores)){
                        clear();
                        System.out.println("Treinador já registrado, pressione Enter"
                                + "para continuar.");
                        pause();
                        buscar_pokemon(pokemons);
                       
                    }
                    else {
                        clear();
                        System.out.println("Treinador não registrado");
                        pause();
                    }                 
                break;
                case "2": listar_pokemon(treinadores.get(k-1).pokemonz);
                                   
                break;
                case "3":
                       registrar(treinadores, pokemons);
                       k++;
                                                
                break;
                case "0": continuar = false;
                break;
                default: 
                    clear();
                    System.out.println("Por favor, digite uma das opções válidas");
                    pause();
                break;            
            }
          }    
    }
  
    public static void clear() {
        for(int i = 0; i < 100; i++)
        {
            System.out.println("");
        } 
    }
    public static void pause() {
        String pause = sc.nextLine().trim();
    }

    private static boolean verifica_treinador(String nom_treinador, 
            ArrayList <Treinador> treinadores) {
        int i=0;
        for (i=0 ; i < treinadores.size(); i++) {
            if(nom_treinador == treinadores.get(i).getNome()) {
                return true; 
            }
        }
        return false;
    }
}
